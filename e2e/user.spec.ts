process.env.NODE_ENV = "development";
process.env.test = "true";
import * as request from "supertest";

describe("User testing", () => {
  let server: request.SuperTest<request.Test>;
  const port = 3000;
  const host = "http://localhost";
  const site = `${host}:${port}`;
  beforeAll(async () => {
    server = request(site);
  });
  let jwtSuperAdmin = null;
  let uuidSuperAdmin = null;
  let jwtdUser = null;
  let uuidUser = null;

  it("/auth/login", async () => {
    const test = await server
      .post("/auth/login")
      .send({ username: "superadmin", password: "kY@@%M$UJ1pt" })
      .set("content-type", "application/json");

    expect(test.body).toEqual(
      expect.objectContaining({
        access_token: expect.any(String),
        expiration: expect.any(Number),
        uuid: expect.any(String),
      })
    );
    expect(test.statusCode).toBe(201);
    jwtSuperAdmin = test.body.access_token;
    uuidSuperAdmin = test.body.uuid;
  });

  it("/user/add", async () => {
    console.log(`Bearer ${jwtSuperAdmin}`);
    // авторизованный доступ
    const add = await server
      .post("/user/add")
      .send({
        username: "tttеttt111",
        password: "123456",
        role: "user",
        info: {
          firstname: "firstname",
          secondname: "secondname",
          patronymic: "patronymic",
          email: "ee",
        },
      })
      .set("authorization", `Bearer ${jwtSuperAdmin}`)
      .set("content-type", "application/json")
      .expect(201);

    // проверка что можем войти
    const testUser = await server
      .post("/auth/login")
      .send({ username: "tttеttt111", password: "123456" })
      .set("content-type", "application/json");

    expect(testUser.body).toEqual(
      expect.objectContaining({
        access_token: expect.any(String),
        expiration: expect.any(Number),
        uuid: expect.any(String),
      })
    );
    console.log(testUser.body);
    jwtdUser = testUser.body.access_token;
    uuidUser = testUser.body.uuid;
    console.log(uuidUser, "Dss");
    expect(testUser.statusCode).toBe(201);

    // неавторизованный доступ
    await server
      .post("/user/add")
      .send({
        username: "tttеttt111",
        password: "123456",
        role: "user",
        info: {
          firstname: "firstname",
          secondname: "secondname",
          patronymic: "patronymic",
          email: "ee",
        },
      })

      .set("content-type", "application/json")
      .expect(403);
  });

  it(`/user/info/edit/${uuidUser}`, async () => {
    console.log(`/user/info/edit/${uuidUser}`);
    //может редактировать сам
    const edit = await server
      .post(`/user/info/edit/${uuidUser}`)
      .set("authorization", `Bearer ${jwtdUser}`)
      .send({
        firstname: "rrrr",
      });
    console.log(edit);
    expect(edit.statusCode).toBe(204);
    // получение инфы о юзере
  });
});
