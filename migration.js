// eslint-disable-next-line @typescript-eslint/no-var-requires
require("dotenv").config({ path: `${process.env.NODE_ENV}.env` });
// eslint-disable-next-line @typescript-eslint/no-var-requires
const fs = require("fs");

const dir = fs.readdirSync(__dirname);
dir.forEach((e) => {
  if (/\d+-migration.js/.test(e)) {
    try {
      console.log(`removing: ${__dirname}/${e}`);
      //fs.unlinkSync(`${__dirname}/${e}`)
    } catch {}
  }
});

fs.writeFileSync(
  "ormconfig.json",
  JSON.stringify({
    type: "postgres",
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    entities: ["dist/entity/*.js"],
    migrations: ["./db.migration.js"],
    synchronize: true,
  })
);
