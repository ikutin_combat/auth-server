"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var DbModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.DbModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const utilities_1 = require("../utilities/utilities");
const dynamicEntities = () => {
    var _a;
    if (((_a = process.env) === null || _a === void 0 ? void 0 : _a.test) === "true") {
        return `${process.cwd()}/src/entity/**/*.{ts,js}`;
    }
    return `${process.cwd()}/dist/entity/**/*.{ts,js}`;
};
let DbModule = DbModule_1 = class DbModule {
    static register() {
        const config = utilities_1.Utilities.getEnvironment();
        return {
            module: DbModule_1,
            imports: [
                typeorm_1.TypeOrmModule.forRoot({
                    type: "postgres",
                    host: config.DB_HOST,
                    port: parseInt(config.DB_PORT),
                    username: config.DB_USER,
                    password: config.DB_PASS,
                    database: config.DB_NAME,
                    entities: [dynamicEntities()],
                    synchronize: false,
                }),
            ],
        };
    }
};
DbModule = DbModule_1 = __decorate([
    (0, common_1.Injectable)()
], DbModule);
exports.DbModule = DbModule;
//# sourceMappingURL=db.config.js.map