"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const common_1 = require("@nestjs/common");
const auth_service_1 = require("../auth/auth.service");
const addRoles_1 = require("../decorators/addRoles");
const Role_1 = require("../enums/Role");
const user_service_1 = require("./user.service");
const utilities_1 = require("../utilities/utilities");
const swagger_1 = require("@nestjs/swagger");
const user_entity_1 = require("../entity/user.entity");
const changePasswordBody_model_1 = require("../models/changePasswordBody.model");
const info_entity_1 = require("../entity/info.entity");
let UserController = class UserController {
    constructor(userService, authService) {
        this.userService = userService;
        this.authService = authService;
    }
    async add(body, req) {
        var _a, _b;
        const token = (_b = (_a = req.headers.authorization.match(/Bearer\s(?<token>.+)/)) === null || _a === void 0 ? void 0 : _a.groups) === null || _b === void 0 ? void 0 : _b.token;
        const payload = this.authService.checkJwt(token);
        try {
            await this.userService.add(body, payload.sub);
        }
        catch (e) {
            throw new common_1.HttpException({ status: common_1.HttpStatus.BAD_REQUEST, error: e.message }, common_1.HttpStatus.BAD_REQUEST);
        }
        return "";
    }
    async edit(userId, req, body) {
        try {
            utilities_1.Utilities.Owner(req === null || req === void 0 ? void 0 : req.user, userId);
            const result = await this.userService.infoEdit(userId, body);
            if (!result) {
                throw new common_1.InternalServerErrorException(`Update info by userUuid  ${userId} is failed with body ${JSON.stringify(body)}`);
            }
        }
        catch (e) {
            const errors = [common_1.ForbiddenException, common_1.InternalServerErrorException];
            if (!errors.some((err) => e instanceof err)) {
                throw new common_1.HttpException({ status: common_1.HttpStatus.BAD_REQUEST, error: e.message }, common_1.HttpStatus.BAD_REQUEST);
            }
            throw e;
        }
    }
    async getInfo(userId, req) {
        console.log(userId);
        try {
            utilities_1.Utilities.Owner(req === null || req === void 0 ? void 0 : req.user, userId);
            const result = await this.userService.infoGet(userId);
            if (!result) {
                throw new common_1.NotFoundException(`User ${userId} is not found`);
            }
            return result;
        }
        catch (e) {
            const errors = [
                common_1.ForbiddenException,
                common_1.InternalServerErrorException,
                common_1.NotFoundException,
            ];
            if (!errors.some((err) => e instanceof err)) {
                throw new common_1.HttpException({ status: common_1.HttpStatus.BAD_REQUEST, error: e.message }, common_1.HttpStatus.BAD_REQUEST);
            }
            throw e;
        }
    }
    async changePassword(userId, req, body) {
        try {
            utilities_1.Utilities.Owner(req === null || req === void 0 ? void 0 : req.user, userId);
            const result = await this.userService.changePassword(userId, body === null || body === void 0 ? void 0 : body.password);
            if (!result) {
                throw new common_1.InternalServerErrorException(`Update info by userUuid  ${userId} is failed with body ${JSON.stringify(body)}`);
            }
        }
        catch (e) {
            const errors = [common_1.ForbiddenException, common_1.InternalServerErrorException];
            if (!errors.some((err) => e instanceof err)) {
                throw new common_1.HttpException({ status: common_1.HttpStatus.BAD_REQUEST, error: e.message }, common_1.HttpStatus.BAD_REQUEST);
            }
            throw e;
        }
    }
};
__decorate([
    (0, common_1.Post)("add"),
    (0, addRoles_1.addRoles)(Role_1.Role.Admin),
    (0, swagger_1.ApiBody)({
        description: "Данные о юзере",
        type: user_entity_1.User,
        required: true,
    }),
    (0, swagger_1.ApiOperation)({
        summary: "Создание юзера",
        description: "Создавать юзеров могут только админы или суперадмины",
    }),
    (0, swagger_1.ApiBearerAuth)("login"),
    (0, swagger_1.ApiCreatedResponse)({
        description: "Юзер создан",
    }),
    (0, swagger_1.ApiBadRequestResponse)({
        description: "Не валидный токен или неполная инфа о юзере",
    }),
    (0, swagger_1.ApiForbiddenResponse)({
        description: "Аунтификация не прошла",
    }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "add", null);
__decorate([
    (0, common_1.Post)("info/edit/:userId"),
    (0, common_1.HttpCode)(204),
    (0, swagger_1.ApiBearerAuth)("login"),
    (0, swagger_1.ApiOperation)({
        summary: "Редактирование юзера",
        description: "Редактировать юзера может только сам юзер или админ или супер админ",
    }),
    (0, swagger_1.ApiParam)({
        type: String,
        name: "userId",
        description: "Uuid юзера которого необходимо авторизовать",
    }),
    (0, swagger_1.ApiBody)({
        type: info_entity_1.Info,
    }),
    (0, swagger_1.ApiNoContentResponse)({
        description: "Пользватель изменен",
    }),
    (0, swagger_1.ApiBadRequestResponse)({
        description: "Не переданы все параметры",
    }),
    (0, swagger_1.ApiForbiddenResponse)({
        description: "Не хватает прав для редактирования",
    }),
    __param(0, (0, common_1.Param)("userId")),
    __param(1, (0, common_1.Req)()),
    __param(2, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "edit", null);
__decorate([
    (0, common_1.Get)("info/get/:userId"),
    (0, swagger_1.ApiBearerAuth)("login"),
    (0, common_1.HttpCode)(200),
    (0, swagger_1.ApiParam)({
        type: String,
        name: "userId",
        description: "Uuid юзера инфу о котром необходимо получить",
    }),
    (0, swagger_1.ApiOperation)({
        summary: "Получение инфы о юзере по uuid ",
        description: "Получить юзера может только сам юзер или админ или супер админ",
    }),
    (0, swagger_1.ApiForbiddenResponse)({
        description: "Не хватает прав для отображения инфы о юзере",
    }),
    (0, swagger_1.ApiOkResponse)({
        type: user_entity_1.User,
    }),
    __param(0, (0, common_1.Param)("userId")),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "getInfo", null);
__decorate([
    (0, swagger_1.ApiBearerAuth)("login"),
    (0, swagger_1.ApiOperation)({
        summary: "Смена пароля у юзера",
        description: "Изменить пароль у юзера может только сам юзер или админ или супер админ",
    }),
    (0, swagger_1.ApiParam)({
        type: String,
        name: "userId",
        description: "Uuid юзера у которого необходимо сменить пароль",
    }),
    (0, swagger_1.ApiBody)({
        type: changePasswordBody_model_1.ChangePasswordBody,
    }),
    (0, swagger_1.ApiOkResponse)({
        description: "Пароль успешно изменен",
    }),
    (0, swagger_1.ApiForbiddenResponse)({
        description: "Не хватает прав для смены  пароля у юзера",
    }),
    (0, common_1.HttpCode)(200),
    (0, common_1.Post)("/changePassword/:userId"),
    __param(0, (0, common_1.Param)("userId")),
    __param(1, (0, common_1.Req)()),
    __param(2, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "changePassword", null);
UserController = __decorate([
    (0, common_1.Controller)("user"),
    (0, swagger_1.ApiTags)("User"),
    __metadata("design:paramtypes", [user_service_1.UserService,
        auth_service_1.AuthService])
], UserController);
exports.UserController = UserController;
//# sourceMappingURL=user.controller.js.map