import { AuthService } from "src/auth/auth.service";
import { UserService } from "./user.service";
import { Info } from "src/entity/info.entity";
export declare class UserController {
    private readonly userService;
    private readonly authService;
    constructor(userService: UserService, authService: AuthService);
    add(body: any, req: any): Promise<string>;
    edit(userId: string, req: any, body: any): Promise<void>;
    getInfo(userId: string, req: any): Promise<Info>;
    changePassword(userId: string, req: any, body: any): Promise<void>;
}
