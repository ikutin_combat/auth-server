"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const info_entity_1 = require("../entity/info.entity");
const user_entity_1 = require("../entity/user.entity");
const crypto = require("crypto");
const Role_1 = require("../enums/Role");
let UserService = class UserService {
    async add(params, uuid) {
        const { info: infoJson } = params, userJson = __rest(params, ["info"]);
        const currentUser = (await user_entity_1.User.find({ where: { uuid: uuid } }))[0];
        const user = new user_entity_1.User().map(userJson);
        const info = new info_entity_1.Info().map(infoJson);
        if ((user.role === Role_1.Role.Admin || user.role === Role_1.Role.SuperAdmin) &&
            currentUser.role !== Role_1.Role.SuperAdmin)
            throw new Error(`Couldn't create role ${user.role}`);
        if ((await user_entity_1.User.findAndCount({
            where: { username: user.username },
        }))[1] > 0) {
            throw new Error(`Username ${user.username} has alredy exists`);
        }
        info.userUuid = user.uuid;
        user.password = crypto
            .pbkdf2Sync(user.password, "", 1000, 64, `sha512`)
            .toString(`hex`);
        user.info = info;
        if (user.validate()) {
            await user.save();
        }
    }
    async infoEdit(userId, data) {
        const resupdate = await info_entity_1.Info.update({ userUuid: userId }, data);
        if (resupdate.affected > 0)
            return true;
        return false;
    }
    async infoGet(userId) {
        console.log({ userUuid: userId });
        const res = await info_entity_1.Info.findOne({ userUuid: userId });
        if (!res)
            return false;
        return res;
    }
    async changePassword(userId, password) {
        if (!password)
            return false;
        const resupdate = await user_entity_1.User.update({ uuid: userId }, {
            password: crypto
                .pbkdf2Sync(password, "", 1000, 64, `sha512`)
                .toString(`hex`),
        });
        if (resupdate.affected > 0)
            return true;
        return false;
    }
};
UserService = __decorate([
    (0, common_1.Injectable)()
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map