import { Info } from "src/entity/info.entity";
export declare class UserService {
    add(params: any, uuid: string): Promise<void>;
    infoEdit(userId: string, data: any): Promise<boolean>;
    infoGet(userId: string): Promise<false | Info>;
    changePassword(userId: string, password: string): Promise<boolean>;
}
