import { User } from "src/entity/user.entity";
import { JwtService } from "src/jwt/jwt.service";
import { JwtPayload } from "jsonwebtoken";
export declare class AuthService {
    private jwtService;
    constructor(jwtService: JwtService);
    validateUser(username: string, password: string): Promise<false | User>;
    login(user: User): Promise<{
        uuid: string;
        access_token: string;
        expiration: number;
    }>;
    checkJwt(jwt: string): JwtPayload;
}
