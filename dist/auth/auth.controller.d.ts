import { AuthService } from "./auth.service";
export declare class AuthController {
    private readonly authService;
    constructor(authService: AuthService);
    login(req: any): Promise<{
        uuid: string;
        access_token: string;
        expiration: number;
    }>;
    checkJwt(req: any): import("jsonwebtoken").JwtPayload;
}
