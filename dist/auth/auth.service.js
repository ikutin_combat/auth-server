"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const common_1 = require("@nestjs/common");
const user_entity_1 = require("../entity/user.entity");
const crypto_1 = require("crypto");
const jwt_service_1 = require("../jwt/jwt.service");
let AuthService = class AuthService {
    constructor(jwtService) {
        this.jwtService = jwtService;
    }
    async validateUser(username, password) {
        const user = await user_entity_1.User.find({
            where: { username },
            select: Object.keys(user_entity_1.User.getRepository().metadata.propertiesMap),
        });
        if (user.length === 0)
            return false;
        if (user[0].password ===
            (0, crypto_1.pbkdf2Sync)(password, "", 1000, 64, `sha512`).toString(`hex`)) {
            const newUser = user[0];
            delete newUser.password;
            return newUser;
        }
    }
    async login(user) {
        const payload = {
            username: user.username,
            sub: user.uuid,
            role: user.role,
        };
        return {
            uuid: user.uuid,
            access_token: this.jwtService.sign(payload),
            expiration: Math.round(new Date().getTime() / 1000) +
                this.jwtService.options.expiration,
        };
    }
    checkJwt(jwt) {
        return this.jwtService.verify(jwt);
    }
};
AuthService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [jwt_service_1.JwtService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map