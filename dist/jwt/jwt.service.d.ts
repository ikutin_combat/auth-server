import * as jwt from "jsonwebtoken";
export interface JwtOptions {
    secret: string;
    expiration: number;
}
export declare class JwtService {
    readonly options: JwtOptions;
    constructor(options: JwtOptions);
    sign(payload: {
        [index: string]: string;
    }): string;
    verify(token: string): string | jwt.JwtPayload;
}
