"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.JwtModule = void 0;
const jwt_service_1 = require("./jwt.service");
class JwtModule {
    static register(options) {
        return {
            module: JwtModule,
            providers: [{ provide: "JWT_OPTIONS", useValue: options }, jwt_service_1.JwtService],
            exports: [jwt_service_1.JwtService],
        };
    }
}
exports.JwtModule = JwtModule;
//# sourceMappingURL=jwt.module.js.map