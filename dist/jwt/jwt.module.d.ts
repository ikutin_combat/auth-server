import { DynamicModule } from "@nestjs/common";
import { JwtOptions } from "./jwt.service";
export declare class JwtModule {
    static register(options: JwtOptions): DynamicModule;
}
