export declare enum Role {
    Admin = "admin",
    User = "user",
    SuperAdmin = "superadmin"
}
