"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Role = void 0;
var Role;
(function (Role) {
    Role["Admin"] = "admin";
    Role["User"] = "user";
    Role["SuperAdmin"] = "superadmin";
})(Role = exports.Role || (exports.Role = {}));
//# sourceMappingURL=Role.js.map