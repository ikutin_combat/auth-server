"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Utilities = exports.Environments = void 0;
const common_1 = require("@nestjs/common");
const dotenv = require("dotenv");
const fs = require("fs");
const Role_1 = require("../enums/Role");
class Environments {
    constructor() {
        this.DB_HOST = "";
        this.DB_PORT = "";
        this.DB_USER = "";
        this.DB_PASS = "";
        this.DB_NAME = "";
        this.SERVER_PORT = "";
    }
}
exports.Environments = Environments;
class Utilities {
    static getEnvPath() {
        return `${process.cwd()}/${process.env.NODE_ENV}.env`;
    }
    static getEnvironment() {
        return dotenv.parse(fs.readFileSync(Utilities.getEnvPath()));
    }
    static Owner(reqUser, userId) {
        var _a, _b;
        const TrustedRoles = [Role_1.Role.Admin, Role_1.Role.SuperAdmin, Role_1.Role.User];
        if (reqUser.error)
            throw new common_1.ForbiddenException(reqUser.error.message);
        if ((((_a = reqUser.info) === null || _a === void 0 ? void 0 : _a.role) === Role_1.Role.User && ((_b = reqUser.payload) === null || _b === void 0 ? void 0 : _b.sub) !== userId) ||
            !TrustedRoles.includes(reqUser.info.role))
            throw new common_1.ForbiddenException();
    }
}
exports.Utilities = Utilities;
//# sourceMappingURL=utilities.js.map