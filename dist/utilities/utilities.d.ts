export declare class Environments {
    DB_HOST: string;
    DB_PORT: string;
    DB_USER: string;
    DB_PASS: string;
    DB_NAME: string;
    SERVER_PORT: string;
}
export declare class Utilities {
    static getEnvPath(): string;
    static getEnvironment(): Environments;
    static Owner(reqUser: any, userId: any): void;
}
