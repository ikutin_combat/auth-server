export declare class LoginRes {
    access_token: string;
    expiration: string;
}
