"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkJwtRes = void 0;
const swagger_1 = require("@nestjs/swagger");
const Role_1 = require("../enums/Role");
class checkJwtRes {
}
__decorate([
    (0, swagger_1.ApiResponseProperty)({
        type: String,
        example: "superadmin",
    }),
    (0, swagger_1.ApiResponseProperty)({}),
    __metadata("design:type", String)
], checkJwtRes.prototype, "username", void 0);
__decorate([
    (0, swagger_1.ApiResponseProperty)({
        enum: Role_1.Role,
    }),
    __metadata("design:type", String)
], checkJwtRes.prototype, "role", void 0);
__decorate([
    (0, swagger_1.ApiResponseProperty)({
        type: String,
        example: "55cc3a8c-e102-43fa-96d4-694a9eff017d",
    }),
    __metadata("design:type", String)
], checkJwtRes.prototype, "sub", void 0);
__decorate([
    (0, swagger_1.ApiResponseProperty)({
        type: Number,
        example: 1646950289,
    }),
    __metadata("design:type", Number)
], checkJwtRes.prototype, "iat", void 0);
__decorate([
    (0, swagger_1.ApiResponseProperty)({
        type: Number,
        example: 1647036689,
    }),
    __metadata("design:type", Number)
], checkJwtRes.prototype, "exp", void 0);
exports.checkJwtRes = checkJwtRes;
//# sourceMappingURL=checkJwtRes.model.js.map