import { Role } from "src/enums/Role";
export declare class checkJwtRes {
    username: string;
    role: Role;
    sub: string;
    iat: number;
    exp: number;
}
