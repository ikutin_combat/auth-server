"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addRoles = exports.SpecificTypes = void 0;
const common_1 = require("@nestjs/common");
var SpecificTypes;
(function (SpecificTypes) {
    SpecificTypes["Deferred"] = "deferred";
})(SpecificTypes = exports.SpecificTypes || (exports.SpecificTypes = {}));
const addRoles = (...roles) => (0, common_1.SetMetadata)("Roles", roles);
exports.addRoles = addRoles;
//# sourceMappingURL=addRoles.js.map