import { Role } from "src/enums/Role";
export declare enum SpecificTypes {
    Deferred = "deferred"
}
export declare const addRoles: (...roles: (Role | SpecificTypes)[]) => import("@nestjs/common").CustomDecorator<string>;
