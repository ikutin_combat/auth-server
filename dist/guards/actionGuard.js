"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ActionGuard = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const Role_1 = require("../enums/Role");
const auth_service_1 = require("../auth/auth.service");
const addRoles_1 = require("../decorators/addRoles");
let ActionGuard = class ActionGuard {
    constructor(reflector, authService) {
        this.reflector = reflector;
        this.authService = authService;
    }
    async canActivate(context) {
        var _a, _b;
        const roles = this.reflector.getAllAndOverride("Roles", [context.getHandler(), context.getClass()]);
        try {
            if (typeof roles !== "undefined" && roles.length > 0) {
                roles.push(Role_1.Role.SuperAdmin);
            }
            else
                return true;
            const request = context.switchToHttp().getRequest();
            if ((_a = request.user) === null || _a === void 0 ? void 0 : _a.error)
                throw request.user.error;
            const payload = (_b = request.user) === null || _b === void 0 ? void 0 : _b.payload;
            if (payload) {
                const userInfo = request.user.info;
                if (userInfo && roles.includes(userInfo.role))
                    return true;
            }
            if (roles.includes(addRoles_1.SpecificTypes.Deferred)) {
                return true;
            }
            return false;
        }
        catch (e) {
            throw new common_1.HttpException({ status: common_1.HttpStatus.FORBIDDEN, error: e.message }, common_1.HttpStatus.FORBIDDEN);
        }
    }
};
ActionGuard = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [core_1.Reflector,
        auth_service_1.AuthService])
], ActionGuard);
exports.ActionGuard = ActionGuard;
//# sourceMappingURL=actionGuard.js.map