import { ExecutionContext } from "@nestjs/common";
import { AuthService } from "src/auth/auth.service";
export declare class OwnerGuard {
    private readonly authService;
    constructor(authService: AuthService);
    canActivate(context: ExecutionContext): boolean;
}
