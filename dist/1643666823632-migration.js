"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.migration1643666823632 = void 0;
class migration1643666823632 {
    constructor() {
        this.name = 'migration1643666823632';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "user" ("id" SERIAL NOT NULL, "uuid" uuid NOT NULL, "password" character varying NOT NULL, "username" character varying NOT NULL, "role" character varying NOT NULL, CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "info" ("id" SERIAL NOT NULL, "userUuid" character varying NOT NULL, "firsName" character varying NOT NULL, "secondName" character varying NOT NULL, "patronymic" character varying NOT NULL, CONSTRAINT "PK_687dc5e25f4f1ee093a45b68bb7" PRIMARY KEY ("id"))`);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE "info"`);
        await queryRunner.query(`DROP TABLE "user"`);
    }
}
exports.migration1643666823632 = migration1643666823632;
//# sourceMappingURL=1643666823632-migration.js.map