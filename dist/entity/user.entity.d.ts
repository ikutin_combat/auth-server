import { Role } from "src/enums/Role";
import { BaseEntity } from "./BaseEntity";
import { Info } from "./info.entity";
export declare class User extends BaseEntity {
    constructor();
    id: number;
    uuid: string;
    password: string;
    username: string;
    role: Role;
    info: Info;
    validate(): boolean;
}
