"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Info = void 0;
const swagger_1 = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const BaseEntity_1 = require("./BaseEntity");
const user_entity_1 = require("./user.entity");
let Info = class Info extends BaseEntity_1.BaseEntity {
    constructor() {
        super();
        this.userUuid = null;
        this.firstname = null;
        this.secondname = null;
        this.email = null;
        this.patronymic = null;
        this.user = null;
    }
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)("increment"),
    __metadata("design:type", Number)
], Info.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: "uuid", primary: true, unique: true }),
    __metadata("design:type", String)
], Info.prototype, "userUuid", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    (0, swagger_1.ApiProperty)({
        type: String,
        example: "firstname",
    }),
    __metadata("design:type", String)
], Info.prototype, "firstname", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    (0, swagger_1.ApiProperty)({
        type: String,
        example: "secondname",
    }),
    __metadata("design:type", String)
], Info.prototype, "secondname", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    (0, swagger_1.ApiProperty)({
        type: String,
        example: "email",
    }),
    __metadata("design:type", String)
], Info.prototype, "email", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    (0, swagger_1.ApiProperty)({
        type: String,
        example: "patronymic",
    }),
    __metadata("design:type", String)
], Info.prototype, "patronymic", void 0);
__decorate([
    (0, typeorm_1.OneToOne)(() => user_entity_1.User, (u) => u.info),
    __metadata("design:type", user_entity_1.User)
], Info.prototype, "user", void 0);
Info = __decorate([
    (0, typeorm_1.Entity)(),
    __metadata("design:paramtypes", [])
], Info);
exports.Info = Info;
//# sourceMappingURL=info.entity.js.map