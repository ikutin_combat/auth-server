import { BaseEntity as _BaseEntity } from "typeorm";
export declare class BaseEntity extends _BaseEntity {
    constructor();
    map(obj: {
        [index: string]: string;
    }): this;
}
