"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
const swagger_1 = require("@nestjs/swagger");
const Role_1 = require("../enums/Role");
const typeorm_1 = require("typeorm");
const uuid = require("uuid");
const BaseEntity_1 = require("./BaseEntity");
const info_entity_1 = require("./info.entity");
let User = class User extends BaseEntity_1.BaseEntity {
    constructor() {
        super();
        this.uuid = null;
        this.password = null;
        this.username = null;
        this.role = null;
        this.info = null;
        this.uuid = uuid.v4();
    }
    validate() {
        var _a, _b, _c, _d, _e, _f, _g, _h;
        const rolesValue = Object.values(Role_1.Role);
        if (this.username === null || this.username === "")
            throw new Error(`Field username is required`);
        if (this.role === null || !rolesValue.includes(this.role))
            throw new Error(`Field role must have ${rolesValue}`);
        if (!this.info)
            throw new Error(`Object info is not received`);
        if (((_a = this.info) === null || _a === void 0 ? void 0 : _a.firstname) === null || ((_b = this.info) === null || _b === void 0 ? void 0 : _b.firstname) === "")
            throw new Error(`Field info.firstname is required`);
        if (((_c = this.info) === null || _c === void 0 ? void 0 : _c.secondname) === null || ((_d = this.info) === null || _d === void 0 ? void 0 : _d.secondname) === "")
            throw new Error(`Field info.secondname is required`);
        if (((_e = this.info) === null || _e === void 0 ? void 0 : _e.patronymic) === null || ((_f = this.info) === null || _f === void 0 ? void 0 : _f.patronymic) === "")
            throw new Error(`Field info.patronymic is required`);
        if (((_g = this.info) === null || _g === void 0 ? void 0 : _g.email) === null || ((_h = this.info) === null || _h === void 0 ? void 0 : _h.email) === "")
            throw new Error(`Field info.email is required`);
        return true;
    }
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)("increment"),
    __metadata("design:type", Number)
], User.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: "uuid", unique: true }),
    __metadata("design:type", String)
], User.prototype, "uuid", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: String,
        example: "1234",
    }),
    (0, typeorm_1.Column)({ select: false }),
    __metadata("design:type", String)
], User.prototype, "password", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: String,
        example: "user",
    }),
    (0, typeorm_1.Column)({ unique: true }),
    __metadata("design:type", String)
], User.prototype, "username", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: Role_1.Role,
        example: "user",
        enum: Role_1.Role,
    }),
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], User.prototype, "role", void 0);
__decorate([
    (0, typeorm_1.OneToOne)(() => info_entity_1.Info, (i) => i.user, {
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
        cascade: true,
    }),
    (0, typeorm_1.JoinColumn)({ name: "uuid", referencedColumnName: "userUuid" }),
    (0, swagger_1.ApiProperty)({
        type: info_entity_1.Info,
    }),
    __metadata("design:type", info_entity_1.Info)
], User.prototype, "info", void 0);
User = __decorate([
    (0, typeorm_1.Entity)("users"),
    __metadata("design:paramtypes", [])
], User);
exports.User = User;
//# sourceMappingURL=user.entity.js.map