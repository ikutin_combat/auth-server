import { BaseEntity } from "./BaseEntity";
import { User } from "./user.entity";
export declare class Info extends BaseEntity {
    constructor();
    id: number;
    userUuid: string;
    firstname: string;
    secondname: string;
    email: string;
    patronymic: string;
    user: User;
}
