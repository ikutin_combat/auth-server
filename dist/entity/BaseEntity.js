"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseEntity = void 0;
const typeorm_1 = require("typeorm");
class BaseEntity extends typeorm_1.BaseEntity {
    constructor() {
        super();
    }
    map(obj) {
        for (const p in obj) {
            if (typeof this[p] !== "undefined")
                this[p] = obj[p];
        }
        return this;
    }
}
exports.BaseEntity = BaseEntity;
//# sourceMappingURL=BaseEntity.js.map