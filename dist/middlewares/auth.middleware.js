"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthMiddleware = void 0;
const common_1 = require("@nestjs/common");
const auth_service_1 = require("../auth/auth.service");
const user_entity_1 = require("../entity/user.entity");
let AuthMiddleware = class AuthMiddleware {
    constructor(authService) {
        this.authService = authService;
    }
    async use(req, res, next) {
        var _a, _b, _c;
        const authorization = req.headers.authorization;
        if (typeof req.user === "undefined") {
            req.user = {};
        }
        if (authorization) {
            const token = (_b = (_a = authorization.match(/Bearer\s(?<token>.+)/)) === null || _a === void 0 ? void 0 : _a.groups) === null || _b === void 0 ? void 0 : _b.token;
            if (token) {
                try {
                    const payload = this.authService.checkJwt(token);
                    const user = (_c = (await user_entity_1.User.find({ where: { uuid: payload.sub } }))) === null || _c === void 0 ? void 0 : _c[0];
                    req.user.payload = payload;
                    req.user.info = user;
                }
                catch (e) {
                    req.user.error = e;
                }
            }
        }
        else {
            req.user = null;
        }
        next();
    }
};
AuthMiddleware = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [auth_service_1.AuthService])
], AuthMiddleware);
exports.AuthMiddleware = AuthMiddleware;
//# sourceMappingURL=auth.middleware.js.map