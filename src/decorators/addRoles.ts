import { SetMetadata } from "@nestjs/common";
import { Role } from "src/enums/Role";

export enum SpecificTypes {
  Deferred = "deferred",
}

export const addRoles = (...roles: (Role | SpecificTypes)[]) =>
  SetMetadata("Roles", roles);
