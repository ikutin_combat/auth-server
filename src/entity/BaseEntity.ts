import { BaseEntity as _BaseEntity } from "typeorm";

export class BaseEntity extends _BaseEntity {
  constructor() {
    super();
  }
  public map(obj: { [index: string]: string }) {
    for (const p in obj) {
      if (typeof this[p] !== "undefined") this[p] = obj[p];
    }
    return this;
  }
}
