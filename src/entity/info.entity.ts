import { ApiProperty } from "@nestjs/swagger";
import { Entity, Column, PrimaryGeneratedColumn, OneToOne } from "typeorm";
import { BaseEntity } from "./BaseEntity";
import { User } from "./user.entity";

@Entity()
export class Info extends BaseEntity {
  constructor() {
    super();
  }

  @PrimaryGeneratedColumn("increment")
  public id: number;

  @Column({ type: "uuid", primary: true, unique: true })
  public userUuid: string = null;

  @Column()
  @ApiProperty({
    type: String,
    example: "firstname",
  })
  public firstname: string = null;

  @Column()
  @ApiProperty({
    type: String,
    example: "secondname",
  })
  public secondname: string = null;

  @Column()
  @ApiProperty({
    type: String,
    example: "email",
  })
  public email: string = null;

  @Column()
  @ApiProperty({
    type: String,
    example: "patronymic",
  })
  public patronymic: string = null;

  @OneToOne(() => User, (u: User) => u.info)
  user: User = null;
}
