import { ApiProperty } from "@nestjs/swagger";
import { Role } from "src/enums/Role";
import {
  Entity,
  Column,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from "typeorm";

import * as uuid from "uuid";
import { BaseEntity } from "./BaseEntity";
import { Info } from "./info.entity";

@Entity("users")
export class User extends BaseEntity {
  constructor() {
    super();
    this.uuid = uuid.v4();
  }

  @PrimaryGeneratedColumn("increment")
  public id: number;

  @Column({ type: "uuid", unique: true })
  public uuid: string = null;

  @ApiProperty({
    type: String,
    example: "1234",
  })
  @Column({ select: false })
  public password: string = null;

  @ApiProperty({
    type: String,
    example: "user",
  })
  @Column({ unique: true })
  public username: string = null;

  @ApiProperty({
    type: Role,
    example: "user",
    enum: Role,
  })
  @Column()
  public role: Role = null;

  @OneToOne(() => Info, (i: Info) => i.user, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
    cascade: true,
  })
  @JoinColumn({ name: "uuid", referencedColumnName: "userUuid" })
  @ApiProperty({
    type: Info,
  })
  public info: Info = null;

  validate(): boolean {
    const rolesValue = Object.values(Role);

    if (this.username === null || this.username === "")
      throw new Error(`Field username is required`);
    if (this.role === null || !rolesValue.includes(this.role))
      throw new Error(`Field role must have ${rolesValue}`);
    if (!this.info) throw new Error(`Object info is not received`);

    if (this.info?.firstname === null || this.info?.firstname === "")
      throw new Error(`Field info.firstname is required`);
    if (this.info?.secondname === null || this.info?.secondname === "")
      throw new Error(`Field info.secondname is required`);
    if (this.info?.patronymic === null || this.info?.patronymic === "")
      throw new Error(`Field info.patronymic is required`);
    if (this.info?.email === null || this.info?.email === "")
      throw new Error(`Field info.email is required`);
    return true;
  }
}
