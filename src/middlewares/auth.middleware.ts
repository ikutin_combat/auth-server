import { Injectable, NestMiddleware } from "@nestjs/common";
import { NextFunction } from "express";
import { AuthService } from "src/auth/auth.service";
import { User } from "src/entity/user.entity";

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor(private readonly authService: AuthService) {}
  async use(req: Request, res: Response, next: NextFunction) {
    const authorization = (req.headers as any).authorization as string;
    if (typeof (req as any).user === "undefined") {
      (req as any).user = {};
    }
    if (authorization) {
      const token = authorization.match(/Bearer\s(?<token>.+)/)?.groups?.token;
      if (token) {
        try {
          const payload = this.authService.checkJwt(token);
          const user = (await User.find({ where: { uuid: payload.sub } }))?.[0];

          (req as any).user.payload = payload;
          (req as any).user.info = user;
        } catch (e) {
          (req as any).user.error = e;
        }
      }
    } else {
      (req as any).user = null;
    }
    next();
  }
}
