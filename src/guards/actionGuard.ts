import {
  Injectable,
  CanActivate,
  ExecutionContext,
  HttpException,
  HttpStatus,
} from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { Role } from "src/enums/Role";
import { AuthService } from "src/auth/auth.service";
import { SpecificTypes } from "src/decorators/addRoles";

@Injectable()
export class ActionGuard implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
    private readonly authService: AuthService
  ) {}
  async canActivate(context: ExecutionContext) {
    const roles = this.reflector.getAllAndOverride<(Role | SpecificTypes)[]>(
      "Roles",
      [context.getHandler(), context.getClass()]
    );
    try {
      // суперадмин может все
      if (typeof roles !== "undefined" && roles.length > 0) {
        roles.push(Role.SuperAdmin);
      } else return true;
      const request = context.switchToHttp().getRequest();
      if (request.user?.error) throw request.user.error;
      const payload = request.user?.payload;
      if (payload) {
        const userInfo = request.user.info;
        if (userInfo && roles.includes(userInfo.role)) return true;
      }
      if (roles.includes(SpecificTypes.Deferred)) {
        return true;
      }

      return false;
    } catch (e) {
      throw new HttpException(
        { status: HttpStatus.FORBIDDEN, error: e.message },
        HttpStatus.FORBIDDEN
      );
    }
  }
}
