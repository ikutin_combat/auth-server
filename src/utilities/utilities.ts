import { ForbiddenException } from "@nestjs/common";
import * as dotenv from "dotenv";
import * as fs from "fs";
import { Role } from "src/enums/Role";

export class Environments {
  DB_HOST = "";
  DB_PORT = "";
  DB_USER = "";
  DB_PASS = "";
  DB_NAME = "";
  SERVER_PORT = "";
}

export class Utilities {
  public static getEnvPath() {
    return `${process.cwd()}/${process.env.NODE_ENV}.env`;
  }

  public static getEnvironment() {
    return dotenv.parse(
      fs.readFileSync(Utilities.getEnvPath())
    ) as unknown as Environments;
  }

  public static Owner(reqUser, userId) {
    const TrustedRoles = [Role.Admin, Role.SuperAdmin, Role.User];
    if (reqUser.error) throw new ForbiddenException(reqUser.error.message);

    //редактировать может супер админ, админ может редактировать все, а юзер может редактировать только свою запись
    if (
      (reqUser.info?.role === Role.User && reqUser.payload?.sub !== userId) ||
      !TrustedRoles.includes(reqUser.info.role)
    )
      throw new ForbiddenException();
  }
}
