import { DynamicModule, Injectable } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Utilities } from "src/utilities/utilities";

const dynamicEntities = () => {
  if (process.env?.test === "true") {
    return `${process.cwd()}/src/entity/**/*.{ts,js}`;
  }
  return `${process.cwd()}/dist/entity/**/*.{ts,js}`;
};

@Injectable()
export class DbModule {
  static register(): DynamicModule {
    const config = Utilities.getEnvironment();
    return {
      module: DbModule,
      imports: [
        TypeOrmModule.forRoot({
          type: "postgres",
          host: config.DB_HOST,
          port: parseInt(config.DB_PORT),
          username: config.DB_USER,
          password: config.DB_PASS,
          database: config.DB_NAME,
          entities: [dynamicEntities()],
          synchronize: false,
        }),
      ],
    };
  }
}
