import { Inject, Injectable } from "@nestjs/common";
import * as jwt from "jsonwebtoken";

export interface JwtOptions {
  secret: string;
  expiration: number;
}

@Injectable()
export class JwtService {
  constructor(
    @Inject("JWT_OPTIONS")
    readonly options: JwtOptions
  ) {}
  public sign(payload: { [index: string]: string }) {
    const time = new Date();
    return jwt.sign(
      {
        ...payload,
        iat: Math.floor(time.getTime() / 1000),
        exp: Math.floor(time.getTime() / 1000 + this.options.expiration),
      },
      this.options.secret
    );
  }
  public verify(token: string) {
    return jwt.verify(token, this.options.secret);
  }
}
