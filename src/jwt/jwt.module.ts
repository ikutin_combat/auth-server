import { DynamicModule } from "@nestjs/common";
import { JwtOptions, JwtService } from "./jwt.service";

export class JwtModule {
  static register(options: JwtOptions): DynamicModule {
    return {
      module: JwtModule,
      providers: [{ provide: "JWT_OPTIONS", useValue: options }, JwtService],
      exports: [JwtService],
    };
  }
}
