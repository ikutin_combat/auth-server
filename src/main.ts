import { NestFactory } from "@nestjs/core";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
import { AppModule } from "./app/app.module";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const config = new DocumentBuilder()
    .setTitle("auth-server")
    .setDescription("Сервер для аунтификации и авторизации пользователей")
    .setVersion("1.0")
    .addBearerAuth(
      { type: "http", scheme: "bearer", bearerFormat: "JWT" },
      "login"
    )
    .build();
  const document = SwaggerModule.createDocument(app, config);

  SwaggerModule.setup("sandbox", app, document);

  await app.listen(3000);
}

bootstrap();
