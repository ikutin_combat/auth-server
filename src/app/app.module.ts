import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from "@nestjs/common";
import { APP_GUARD } from "@nestjs/core";
import { AuthModule } from "src/auth/auth.module";
import { DbModule } from "src/config/db.config";
import { ActionGuard } from "src/guards/actionGuard";
import { AuthMiddleware } from "src/middlewares/auth.middleware";
import { UserModule } from "src/user/user.module";

@Module({
  imports: [DbModule.register(), AuthModule, UserModule],
  providers: [{ provide: APP_GUARD, useClass: ActionGuard }],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .forRoutes({ path: "*", method: RequestMethod.ALL });
  }
}
