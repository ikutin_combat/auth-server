import { Injectable } from "@nestjs/common";
import { Info } from "src/entity/info.entity";
import { User } from "src/entity/user.entity";
import * as crypto from "crypto";
import { Role } from "src/enums/Role";

@Injectable()
export class UserService {
  public async add(params: any, uuid: string) {
    const { info: infoJson, ...userJson } = params;
    const currentUser = (await User.find({ where: { uuid: uuid } }))[0];
    const user = new User().map(userJson);
    const info = new Info().map(infoJson);
    if (
      (user.role === Role.Admin || user.role === Role.SuperAdmin) &&
      currentUser.role !== Role.SuperAdmin
    )
      throw new Error(`Couldn't create role ${user.role}`);
    if (
      (
        await User.findAndCount({
          where: { username: user.username },
        })
      )[1] > 0
    ) {
      throw new Error(`Username ${user.username} has alredy exists`);
    }

    info.userUuid = user.uuid;
    user.password = crypto
      .pbkdf2Sync(user.password, "", 1000, 64, `sha512`)
      .toString(`hex`);
    user.info = info;
    if (user.validate()) {
      await user.save();
    }
  }

  public async infoEdit(userId: string, data: any) {
    const resupdate = await Info.update({ userUuid: userId }, data);
    if (resupdate.affected > 0) return true;
    return false;
  }
  public async infoGet(userId: string) {
    console.log({ userUuid: userId });
    const res = await Info.findOne({ userUuid: userId });

    if (!res) return false;
    return res;
  }

  public async changePassword(userId: string, password: string) {
    if (!password) return false;
    const resupdate = await User.update(
      { uuid: userId },
      {
        password: crypto
          .pbkdf2Sync(password, "", 1000, 64, `sha512`)
          .toString(`hex`),
      }
    );
    if (resupdate.affected > 0) return true;
    return false;
  }
}
