import {
  Body,
  Controller,
  ForbiddenException,
  Get,
  HttpCode,
  HttpException,
  HttpStatus,
  InternalServerErrorException,
  NotFoundException,
  Param,
  Post,
  Req,
  Request,
} from "@nestjs/common";
import { AuthService } from "src/auth/auth.service";
import { addRoles } from "src/decorators/addRoles";
import { Role } from "src/enums/Role";
import { UserService } from "./user.service";
import { Utilities } from "src/utilities/utilities";
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiBody,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiNoContentResponse,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiTags,
} from "@nestjs/swagger";
import { User } from "src/entity/user.entity";
import { ChangePasswordBody } from "src/models/changePasswordBody.model";
import { Info } from "src/entity/info.entity";

@Controller("user")
@ApiTags("User")
export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly authService: AuthService
  ) {}

  @Post("add")
  @addRoles(Role.Admin)
  @ApiBody({
    description: "Данные о юзере",
    type: User,
    required: true,
  })
  @ApiOperation({
    summary: "Создание юзера",
    description: "Создавать юзеров могут только админы или суперадмины",
  })
  @ApiBearerAuth("login")
  @ApiCreatedResponse({
    description: "Юзер создан",
  })
  @ApiBadRequestResponse({
    description: "Не валидный токен или неполная инфа о юзере",
  })
  @ApiForbiddenResponse({
    description: "Аунтификация не прошла",
  })
  public async add(@Body() body: any, @Request() req: any) {
    const token = (req.headers.authorization as string).match(
      /Bearer\s(?<token>.+)/
    )?.groups?.token;
    const payload = this.authService.checkJwt(token);

    try {
      await this.userService.add(body, payload.sub);
    } catch (e) {
      throw new HttpException(
        { status: HttpStatus.BAD_REQUEST, error: e.message },
        HttpStatus.BAD_REQUEST
      );
    }

    return "";
  }

  @Post("info/edit/:userId")
  @HttpCode(204)
  @ApiBearerAuth("login")
  @ApiOperation({
    summary: "Редактирование юзера",
    description:
      "Редактировать юзера может только сам юзер или админ или супер админ",
  })
  @ApiParam({
    type: String,
    name: "userId",
    description: "Uuid юзера которого необходимо авторизовать",
  })
  @ApiBody({
    type: Info,
  })
  @ApiNoContentResponse({
    description: "Пользватель изменен",
  })
  @ApiBadRequestResponse({
    description: "Не переданы все параметры",
  })
  @ApiForbiddenResponse({
    description: "Не хватает прав для редактирования",
  })
  public async edit(
    @Param("userId") userId: string,
    @Req() req: any,
    @Body() body: any
  ) {
    try {
      Utilities.Owner(req?.user, userId);

      const result = await this.userService.infoEdit(userId, body);
      if (!result) {
        throw new InternalServerErrorException(
          `Update info by userUuid  ${userId} is failed with body ${JSON.stringify(
            body
          )}`
        );
      }
    } catch (e) {
      const errors = [ForbiddenException, InternalServerErrorException];
      if (!errors.some((err) => e instanceof err)) {
        throw new HttpException(
          { status: HttpStatus.BAD_REQUEST, error: e.message },
          HttpStatus.BAD_REQUEST
        );
      }
      throw e;
    }
  }

  @Get("info/get/:userId")
  @ApiBearerAuth("login")
  @HttpCode(200)
  @ApiParam({
    type: String,
    name: "userId",
    description: "Uuid юзера инфу о котром необходимо получить",
  })
  @ApiOperation({
    summary: "Получение инфы о юзере по uuid ",
    description:
      "Получить юзера может только сам юзер или админ или супер админ",
  })
  @ApiForbiddenResponse({
    description: "Не хватает прав для отображения инфы о юзере",
  })
  @ApiOkResponse({
    type: User,
  })
  public async getInfo(@Param("userId") userId: string, @Req() req: any) {
    console.log(userId);
    try {
      Utilities.Owner(req?.user, userId);

      const result = await this.userService.infoGet(userId);
      if (!result) {
        throw new NotFoundException(`User ${userId} is not found`);
      }
      return result;
    } catch (e) {
      const errors = [
        ForbiddenException,
        InternalServerErrorException,
        NotFoundException,
      ];
      if (!errors.some((err) => e instanceof err)) {
        throw new HttpException(
          { status: HttpStatus.BAD_REQUEST, error: e.message },
          HttpStatus.BAD_REQUEST
        );
      }
      throw e;
    }
  }
  @ApiBearerAuth("login")
  @ApiOperation({
    summary: "Смена пароля у юзера",
    description:
      "Изменить пароль у юзера может только сам юзер или админ или супер админ",
  })
  @ApiParam({
    type: String,
    name: "userId",
    description: "Uuid юзера у которого необходимо сменить пароль",
  })
  @ApiBody({
    type: ChangePasswordBody,
  })
  @ApiOkResponse({
    description: "Пароль успешно изменен",
  })
  @ApiForbiddenResponse({
    description: "Не хватает прав для смены  пароля у юзера",
  })
  @HttpCode(200)
  @Post("/changePassword/:userId")
  public async changePassword(
    @Param("userId") userId: string,
    @Req() req: any,
    @Body() body: any
  ) {
    try {
      Utilities.Owner(req?.user, userId);
      const result = await this.userService.changePassword(
        userId,
        body?.password
      );
      if (!result) {
        throw new InternalServerErrorException(
          `Update info by userUuid  ${userId} is failed with body ${JSON.stringify(
            body
          )}`
        );
      }
    } catch (e) {
      const errors = [ForbiddenException, InternalServerErrorException];
      if (!errors.some((err) => e instanceof err)) {
        throw new HttpException(
          { status: HttpStatus.BAD_REQUEST, error: e.message },
          HttpStatus.BAD_REQUEST
        );
      }
      throw e;
    }
  }
}
