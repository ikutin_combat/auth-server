import {
  Controller,
  Header,
  HttpCode,
  HttpException,
  HttpStatus,
  Post,
  Request,
  UseGuards,
} from "@nestjs/common";
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from "@nestjs/swagger";
import { CheckJwtBody } from "src/models/checkJwtBody.model";
import { checkJwtRes } from "src/models/checkJwtRes.model";
import { LoginBody } from "src/models/loginBody.model";
import { LoginRes } from "src/models/loginRes.model";
import { AuthService } from "./auth.service";
import { LocalAuthGuard } from "./local-auth.guard";

@Controller("auth")
@ApiTags("Auth")
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @Post("login")
  @HttpCode(201)
  @ApiOperation({
    summary: "Авторизация юзера",
    description: "Авторизация юзера",
  })
  @ApiBody({
    type: LoginBody,
  })
  @ApiCreatedResponse({
    description: "Генерация jwt",
    type: LoginRes,
  })
  @ApiForbiddenResponse({
    description: "Ошибка авторизации",
  })
  public login(@Request() req) {
    return this.authService.login((req as any).user);
  }

  @Post("checkJwt")
  @HttpCode(200)
  @ApiOperation({
    summary: "Проверка токена",
    description: "Проверка токена",
  })
  @ApiBody({
    description: "Проверка jwt и возвращение информации о токене",
    type: CheckJwtBody,
  })
  @ApiOkResponse({
    description: "Проверка jwt и возвращение информации о токене",
    type: checkJwtRes,
  })
  @ApiBadRequestResponse({
    description: "Ошибка с токеном",
  })
  public checkJwt(@Request() req) {
    try {
      return this.authService.checkJwt(req.body.jwt);
    } catch (e) {
      throw new HttpException(
        { status: HttpStatus.BAD_REQUEST, error: e.message },
        HttpStatus.BAD_REQUEST
      );
    }
  }
}
