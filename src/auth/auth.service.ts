import { Injectable } from "@nestjs/common";
import { User } from "src/entity/user.entity";
import { pbkdf2Sync } from "crypto";
import { JwtService } from "src/jwt/jwt.service";
import { JwtPayload } from "jsonwebtoken";

@Injectable()
export class AuthService {
  constructor(private jwtService: JwtService) {}

  async validateUser(username: string, password: string) {
    const user = await User.find({
      where: { username },
      select: Object.keys(
        User.getRepository().metadata.propertiesMap
      ) as (keyof User)[],
    });

    if (user.length === 0) return false;
    if (
      user[0].password ===
      pbkdf2Sync(password, "", 1000, 64, `sha512`).toString(`hex`)
    ) {
      const newUser = user[0];
      delete newUser.password;
      return newUser;
    }
  }

  async login(user: User) {
    const payload = {
      username: user.username,
      sub: user.uuid,
      role: user.role,
    };
    return {
      uuid: user.uuid,
      access_token: this.jwtService.sign(payload),
      expiration:
        Math.round(new Date().getTime() / 1000) +
        this.jwtService.options.expiration,
    };
  }
  checkJwt(jwt: string) {
    return this.jwtService.verify(jwt) as JwtPayload;
  }
}
