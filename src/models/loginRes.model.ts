import { ApiProperty } from "@nestjs/swagger";

export class LoginRes {
  @ApiProperty({
    type: String,
    required: true,
  })
  access_token: string;

  @ApiProperty({
    type: String,
    required: true,
  })
  expiration: string;
}
