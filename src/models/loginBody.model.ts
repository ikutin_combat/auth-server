import { ApiProperty } from "@nestjs/swagger";

export class LoginBody {
  @ApiProperty({
    type: String,
    required: true,
    example: "superadmin",
  })
  username: string;
  @ApiProperty({
    type: String,
    required: true,
    example: "kY@@%M$UJ1pt",
  })
  password: string;
}
