import { ApiProperty } from "@nestjs/swagger";

export class ChangePasswordBody {
  @ApiProperty({
    type: String,
  })
  password: string;
}
