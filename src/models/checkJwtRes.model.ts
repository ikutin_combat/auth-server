import { ApiResponseProperty } from "@nestjs/swagger";
import { Role } from "src/enums/Role";

export class checkJwtRes {
  @ApiResponseProperty({
    type: String,
    example: "superadmin",
  })
  @ApiResponseProperty({})
  username: string;

  @ApiResponseProperty({
    enum: Role,
  })
  role: Role;

  @ApiResponseProperty({
    type: String,
    example: "55cc3a8c-e102-43fa-96d4-694a9eff017d",
  })
  sub: string;
  @ApiResponseProperty({
    type: Number,
    example: 1646950289,
  })
  iat: number;
  @ApiResponseProperty({
    type: Number,
    example: 1647036689,
  })
  exp: number;
}
