import { ApiProperty } from "@nestjs/swagger";

export class CheckJwtBody {
  @ApiProperty({
    type: String,
    required: true,
    example:
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyYWRtaW4iLCJzdWIiOiI1NWNjM2E4Yy1lMTAyLTQzZmEtOTZkNC02OTRhOWVmZjAxN2QiLCJpYXQiOjE2NDcwMzQ2NDQsImV4cCI6MTY0NzEyMTA0NH0.QH52VWqhQzNc9eq6baN4FnefEyQFpsyCBBHCKhnqys0",
  })
  jwt: string;
}
