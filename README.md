# auth-server

[Установка](#установка)  
[Запуск](#запуск)  
[Изолированный запуска в докере](#изолированный-запуска-в-докере)  
[api](#api)


## Установка
`npm i`

## Запуск
`npm run docker:db:build` - собирает образ db
`npm run docker:db:run` - запускает дб в котейнере
`typeorm:run` - создает таблицы и главного админа см data.migration.js

## изолированный запуска в докере
`npm run docker:compose:build` - собирает все образы
`npm run docker:compose:run` - запускает и стартует проект на 3000 порту см docker\.env

## Api
`http://localhost:3000/sandbox`

