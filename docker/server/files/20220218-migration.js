const { MigrationInterface, QueryRunner } = require("typeorm");
const Data = require("./data.migration");
module.exports = class migration1645110754024 {
    name = 'migration1645110754024'

    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "users" ("uuid" uuid NOT NULL, "password" character varying NOT NULL, "username" character varying NOT NULL, "role" character varying NOT NULL, CONSTRAINT "UQ_fe0bb3f6520ee0469504521e710" UNIQUE ("username"), CONSTRAINT "REL_951b8f1dfc94ac1d0301a14b7e" UNIQUE ("uuid"), CONSTRAINT "PK_951b8f1dfc94ac1d0301a14b7e1" PRIMARY KEY ("uuid"))`);
        await queryRunner.query(`CREATE TABLE "info" ("id" SERIAL NOT NULL, "userUuid" uuid NOT NULL, "firstname" character varying NOT NULL, "secondname" character varying NOT NULL, "patronymic" character varying NOT NULL, CONSTRAINT "UQ_276437d574a5e651c67207f0f62" UNIQUE ("userUuid"), CONSTRAINT "PK_bb83aa457528a2d21c64751f6e0" PRIMARY KEY ("id", "userUuid"))`);
        await queryRunner.query(`ALTER TABLE "users" ADD CONSTRAINT "FK_951b8f1dfc94ac1d0301a14b7e1" FOREIGN KEY ("uuid") REFERENCES "info"("userUuid") ON DELETE CASCADE ON UPDATE CASCADE`);
        new Data().up(queryRunner);
    }

    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "users" DROP CONSTRAINT "FK_951b8f1dfc94ac1d0301a14b7e1"`);
        await queryRunner.query(`DROP TABLE "info"`);
        await queryRunner.query(`DROP TABLE "users"`);
    }
}
