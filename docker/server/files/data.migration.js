const { MigrationInterface, QueryRunner } = require("typeorm");
const crypto = require("crypto");
const uuid = "55cc3a8c-e102-43fa-96d4-694a9eff017d";

module.exports = class migration1643672196763 {
  name = "migration1643672196763";

  async up(queryRunner) {
    await queryRunner.query(
      `INSERT INTO info ("userUuid", "firstname", "secondname", "patronymic","email") VALUES('${uuid}', 'superadmin', 'superadmin', 'superadmin','email.com')`
    );
    await queryRunner.query(
      `INSERT INTO users ("uuid", "password", "username",  "role") VALUES('${uuid}', '${crypto
        .pbkdf2Sync("kY@@%M$UJ1pt", "", 1000, 64, `sha512`)
        .toString(`hex`)}', 'superadmin', 'superadmin')`
    );
  }

  async down(queryRunner) {
    await queryRunner.query(
      `ALTER TABLE "users" DROP CONSTRAINT "FK_951b8f1dfc94ac1d0301a14b7e1"`
    );
    await queryRunner.query(`DROP TABLE "info"`);
    await queryRunner.query(`DROP TABLE "users"`);
  }
};
