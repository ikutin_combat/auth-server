#usr/bin/bash

con_name="auth_db"
docker rm --force $con_name
docker run  -d    -v "$(pwd)"/psql_data:/volume/psql_data -dit --name $con_name -p 127.0.0.1:5432:5432/tcp $con_name
docker exec -d $con_name bash /start.sh

